package com.example.testforandroid;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

/**
 * 
 * @author beyond
 * QQ:348638799
 * github: https://github.com/beyondxia
 *
 */
public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// this is a test content
		// this is a test content2
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
